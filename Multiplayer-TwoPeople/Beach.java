import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import greenfoot.Greenfoot;

/**
 * Write a description of class Beach here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Beach extends World
{
    int roundsSurvived = 0;
    /**
     * Constructor for objects of class Beach.
     * 
     */
    public Beach()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
    }
    
    public void act() {
        roundsSurvived++;
        int wavePos = 0;

        if (Greenfoot.isKeyDown("1")) {
            wavePos = Greenfoot.getRandomNumber(200);
        } else if (Greenfoot.isKeyDown("2")) {
            wavePos = (Greenfoot.getRandomNumber(200) + 200);
        } else if (Greenfoot.isKeyDown("3")) {
            wavePos = (Greenfoot.getRandomNumber(200) + 400);
        } else {
            wavePos = Greenfoot.getRandomNumber(600);
        }

        if (Greenfoot.getRandomNumber(8) == 0) {
            // this.addObject(new Wave(Greenfoot.getRandomNumber(5) + 1), Greenfoot.getRandomNumber(600), 0);
            int waveType = Greenfoot.getRandomNumber(2);
            switch(waveType) {
                case 0:
                this.addObject(new WaveFull(Greenfoot.getRandomNumber(5) + 1), wavePos, 0);
                break;
                case 1:
                this.addObject(new WaveHalf(Greenfoot.getRandomNumber(5) + 1), wavePos, 0);
                break;
                case 2:
                this.addObject(new WaveQuarter(Greenfoot.getRandomNumber(5) + 1), wavePos, 0);
                break;
            }
        }
    }
    
    public void started() {
        this.removeObjects(this.getObjects(Wave.class));
        this.removeObjects(this.getObjects(Person.class));
        showText("", 300, 125);
        roundsSurvived = 0;
        this.addObject(new PlayerOne(), 200, 325);
        this.addObject(new PlayerTwo(), 400, 325);
    }
    
    public void stopped() {
        int deadPlayer = (this.getObjects(PlayerOne.class)).isEmpty() ? 1 : 2;
        
        showText("Game stopped.\nIdea By Sven Eschlbeck.\nRealised by Adrian Welcker and Sven Eschlbeck.\n\n" +
                 "Player " + deadPlayer  + " died after "+ roundsSurvived + " game rounds\n\n\nDevelopment version." +
                 "\nWe apologize for any bugs.", 300, 125);
        
        this.removeObjects(this.getObjects(Wave.class));
        this.removeObjects(this.getObjects(Person.class));
    }
}
