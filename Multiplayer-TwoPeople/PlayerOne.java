import greenfoot.*;

/**
 * Write a description of class PlayerOne here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PlayerOne extends Person
{
    /**
     * Act - do whatever the PlayerOne wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if (Greenfoot.isKeyDown("a")) {
            this.setLocation(getX() - 4, getY());
        }
        
        if (Greenfoot.isKeyDown("d")) {
            this.setLocation(getX() + 4, getY());
        }
    }    
}
