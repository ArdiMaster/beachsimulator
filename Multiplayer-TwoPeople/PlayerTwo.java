import greenfoot.*;

/**
 * Write a description of class PlayerTwo here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PlayerTwo extends Person
{
    /**
     * Act - do whatever the PlayerTwo wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if (Greenfoot.isKeyDown("left")) {
            this.setLocation(getX() - 4, getY());
        }
        
        if (Greenfoot.isKeyDown("right")) {
            this.setLocation(getX() + 4, getY());
        }
    }    
}
