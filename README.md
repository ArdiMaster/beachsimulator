# README #

BeachSimulator was created as a school project in Java/Greenfoot. You need to escape the waves by moving a person along a beach.

# Version #

There are three different versions of this game available.

## Singleplayer ##

You control the person using the left and right arrow keys. Waves are generated randomly.

## Multiplayer-WaveControl ##

One player controls the person using the left and right arrow keys. Waves are generated randomly, but the generation can be influenced by the second player: If the "1" key is pressed, waves are only generated in the left third of the beach. Pressing and holding "2" results in waves only generating in the middle third of the beach, and holding down "3" results in waves generating only in the right third of the beach.

## Multiplayer-TwoPeople ##

One player controls person one using the A and D keys while the other player controls person two using the left and right arrow keys. Waves are generated randomly.

# Requirements #

To run any of the above versions, you need to have JDK 6 or higher and Greenfoot IDE v2.4.2 or newer installed on your computer. After you download the source code, the three versions are found in three folders, making them three separate IDE projects.

# Notice for my teacher #

This was Sven Eschlbeck's original idea. I created this in collaboration with him.

# License #

```
#!text

Copyright (c) 2015, Adrian "ArdiMaster" Welcker
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this
   list of conditions and the following disclaimer in the documentation and/or
   other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software without
   specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
```