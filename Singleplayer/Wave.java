import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Wave here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public abstract class Wave extends Actor
{
    int stepWidth;
    
    public Wave(int v) {
        super();
        this.stepWidth = v;
    }
    
    /**
     * Act - do whatever the Wave wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        this.setLocation(this.getX(), this.getY() + this.stepWidth);
        
        if (this.getY() >= 399) {
            this.getWorld().removeObject(this);
        }
        
        /* if (this.isTouching(Person.class)) {
            this.removeTouching(Person.class);
            Greenfoot.stop();
        } */
        
        try {
            if (this.getOneIntersectingObject(Person.class) != null) {
                this.getWorld().removeObject(this.getOneIntersectingObject(Person.class));
                Greenfoot.stop();
        }
        } catch (IllegalStateException e) {
            // getWorld().showText("There was some weirdo error, but it was caught.\nPlease go on.", 300, 50);
            // e.printStackTrace();
            /* try {
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            } */
            // this.getWorld().showText("", 300, 50);
        }
        
        /* for (Object obj : this.getObjects(Wave.class) {
            this.removeObject
        } */
        

    }    
}
